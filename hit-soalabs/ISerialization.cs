﻿namespace Labs
{
    interface ISerialization<T, Y>
    {
        string serialization(T output);
        Y deserialization(string input);
    }
}
