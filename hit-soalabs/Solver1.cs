﻿using System;
using System.Collections.Generic;

namespace Labs
{
    class Solver1
    {
        public void solve()
        {
            var typeOfSerialization = Console.ReadLine();
            var inputSerialized = Console.ReadLine();

            var serialize = new Dictionary<string, ISerialization<Output, Input>>() {
                {"Json", new JsonSerialization<Output, Input>()},
                {"Xml", new XmlSerialization<Output, Input>()}
            };

            var input = serialize[typeOfSerialization].deserialization(inputSerialized);
            var output = new Calculator().GenerateOutput(input);
            Console.WriteLine(serialize[typeOfSerialization].serialization(output));
        }
    }
}
