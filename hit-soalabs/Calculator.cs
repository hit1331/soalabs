using System;

namespace Labs
{
    class Calculator : ICalculator
    {
        public Output GenerateOutput(Input input)
        {
            Output output = new Output();
            output.SumResult = 0;
            foreach (var sum in input.Sums)
                output.SumResult += sum;
            output.SumResult *= input.K;

            output.MulResult = 1;
            foreach (var mul in input.Muls)
                output.MulResult *= mul;

            output.SortedInputs = new decimal[input.Sums.Length + input.Muls.Length];
            for (int i = 0; i < input.Sums.Length; ++i)
                output.SortedInputs[i] = input.Sums[i];
            for (int i = 0; i < input.Muls.Length; ++i)
                output.SortedInputs[input.Sums.Length + i] = input.Muls[i];
            Array.Sort(output.SortedInputs);

            return output;
        }
    }
}
