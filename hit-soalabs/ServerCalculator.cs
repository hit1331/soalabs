﻿namespace Labs
{
    class ServerCalculator : IServerCalculator
    {
        public string calculateAnswer(string body)
        {
            var serializer = new JsonSerialization<Output, Input>();
            var input = serializer.deserialization(body);
            return (input == null) ? "" : serializer.serialization(
                new Calculator().GenerateOutput(input));
        }
    }
}
