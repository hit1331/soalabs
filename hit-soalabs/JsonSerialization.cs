﻿using Newtonsoft.Json;

namespace Labs
{
    class JsonSerialization<T, Y> : ISerialization<T, Y>
    {
        public string serialization(T output)
        {
            return JsonConvert.SerializeObject(output);
        }

        public Y deserialization(string obj)
        {
            return JsonConvert.DeserializeObject<Y>(obj);
        }
    }
}
