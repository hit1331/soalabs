﻿using System;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Labs
{
    class XmlSerialization<T, Y> : ISerialization<T, Y>
    {
        public XmlSerialization()
        {
            tSerializer = new XmlSerializer(typeof(T));
            ySerializer = new XmlSerializer(typeof(Y));
        }

        public string serialization(T output)
        {
            var xmlSettings = new XmlWriterSettings();
            xmlSettings.OmitXmlDeclaration = true;
            var xmlSerializerNamespace = new XmlSerializerNamespaces();
            xmlSerializerNamespace.Add("", "");
            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter, xmlSettings))
            {
                tSerializer.Serialize(writer, output, xmlSerializerNamespace);
            }
            return stringWriter.ToString();
        }

        public Y deserialization(string obj)
        {
            using (var reader = XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(obj))))
            {
                return (Y)ySerializer.Deserialize(reader);
            }
        }

        private XmlSerializer tSerializer, ySerializer;
    }
}
